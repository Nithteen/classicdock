#include <substrate.h>
#include <UIKit/UIKit.h>
#include <objc/runtime.h>
#import "BounceAnimation.h"

@interface _SBDockBackgroundView : UIView

@end

@interface SBWallpaperEffectView : UIView

@end

@interface SBHighlightView : UIView
@end

@interface SBDockIconListView : UIView

@end

@interface SBApplication : NSObject
- (NSString *)displayIdentifier;
-(BOOL)isRunning;

@end

@interface SBDockView : UIView

- (UIView *)dockListView;
@end

@interface CSReflectionView : UIImageView

@end

@implementation CSReflectionView

@end

@interface SBIconImageView : UIView
- (UIImage *)snapshot;
- (UIImage *)contentsImage;
- (UIImage *)_currentOverlayImage;
- (float)overlayAlpha;

@end

@interface SBIcon : NSObject
- (void)launchFromLocation:(int)location;
@end

@interface SBIconModel : NSObject
- (SBIcon *)expectedIconForDisplayIdentifier:(NSString *)displayIdentifier;
@end

@interface SBIconController : NSObject
+ (instancetype)sharedInstance;
- (SBIconModel *)model;
@end

@interface SBApplicationIcon : SBIcon
-(SBApplication *)application;
@end

@interface SBIconView : UIView
- (SBApplicationIcon *)icon;
- (BOOL)isInDock;
- (SBIconImageView *)_iconImageView;
- (void)updateReflection;
- (BOOL)isHighlighted;
- (void)bounceWithMagnitude:(float)height duration:(float)duration completion:(void (^)())completion;
@end

@interface SBIconViewMap : NSObject
+ (instancetype) homescreenMap;
- (SBIconView *)mappedIconViewForIcon:(SBIcon *)icon;
@end

static const char *kCSReflectionViewIdentifier;
static const char *kCSReflectionOverlayViewIdentifier;
static const char *kCSRunningGlowViewIdentifier;
BOOL CSRunningIndicatorsEnabled = YES;
BOOL CSReflectionEnabled = YES;
BOOL CSDarkenBackground = YES;
BOOL CSBounceIconOnLaunch = NO;
BOOL CSBouncedAlready = NO;
SBIconView *lastIconView;

@class SBWallpaperEffectView;

%group ClassicDock

%hook SBDockView
- (id)initWithDockListView:(id)dockListView forSnapshot:(BOOL)snapshot {
    self = %orig;
    if (self){
        UIImageView *_backgroundView = MSHookIvar<UIImageView *>(self, "_backgroundView"); //A5 and higher
        if (_backgroundView)
            [_backgroundView removeFromSuperview];
        
        UIImageView *_backgroundImageView = MSHookIvar<UIImageView *>(self, "_backgroundImageView"); //iPhone 4
        if (_backgroundImageView)
            [_backgroundImageView removeFromSuperview];
            
        UIImage *maskImage = [UIImage imageNamed:@"SBDockBG.png"];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            maskImage = [UIImage imageNamed:@"SBDockBG-Landscape.png"];
        CGRect bgFrame = CGRectMake(0,51,self.frame.size.width,45);
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            bgFrame = CGRectMake(0,59,self.frame.size.width,64);
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:bgFrame];
        imgView.image = maskImage;
        imgView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview:[imgView autorelease]];
        [self sendSubviewToBack:imgView];
            
        if (CSDarkenBackground){
            UIImageView *backgroundGradient = [[UIImageView alloc] initWithFrame:self.bounds];
            backgroundGradient.image = [UIImage imageNamed:@"CSDockBGOverlay.png"];
            [backgroundGradient setBackgroundColor:[UIColor clearColor]];
            backgroundGradient.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [self addSubview:[backgroundGradient autorelease]];
            [self sendSubviewToBack:backgroundGradient];
        }
        
        MSHookIvar<UIImageView *>(self, "_backgroundView") = nil; // A5 and higher
        MSHookIvar<UIImageView *>(self, "_backgroundImageView") = nil; // iPhone 4
    }
    return self;
}
- (void)layoutSubviews {
    %orig;
    // A5 and higher
    UIImageView *_backgroundView = MSHookIvar<UIImageView *>(self, "_backgroundView");
    if (_backgroundView){
        [_backgroundView removeFromSuperview];
        MSHookIvar<UIImageView *>(self, "_backgroundView") = nil;
    }
    
    // iPhone 4
    UIImageView *_backgroundImageView = MSHookIvar<UIImageView *>(self, "_backgroundImageView");
    if (_backgroundImageView){
        [_backgroundImageView removeFromSuperview];
        MSHookIvar<UIImageView *>(self, "_backgroundImageView") = nil;
    }
}
%end

%hook SBIconView

%new
+ (id)lastIconView {
    return lastIconView;
}

- (id)initWithDefaultSize {
    self = %orig;
    lastIconView = self;
    
    self.clipsToBounds = NO;
    
    CGRect frame = CGRectMake(2, 59, 62, 62);
    CSReflectionView *reflectionView = [[CSReflectionView alloc] initWithFrame:frame];
    reflectionView.transform = CGAffineTransformMakeScale(1, -1);
    reflectionView.alpha = 0.4;
    reflectionView.contentMode = UIViewContentModeBottom;
    reflectionView.clipsToBounds = YES;
    objc_setAssociatedObject(self, &kCSReflectionViewIdentifier, reflectionView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addSubview:[reflectionView autorelease]];
    
    CSReflectionView *reflectionOverlayView = [[CSReflectionView alloc] initWithFrame:frame];
    reflectionOverlayView.transform = CGAffineTransformMakeScale(1, -1);
    reflectionOverlayView.alpha = 0.4;
    reflectionOverlayView.contentMode = UIViewContentModeBottom;
    reflectionOverlayView.clipsToBounds = YES;
    objc_setAssociatedObject(self, &kCSReflectionOverlayViewIdentifier, reflectionOverlayView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addSubview:[reflectionOverlayView autorelease]];
    
    CGRect indicatorFrame = CGRectMake(20,78,20,5);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        indicatorFrame = CGRectMake(27, 100, 20, 5);
    UIImageView *glowingIndicator = [[UIImageView alloc] initWithFrame:indicatorFrame];
    glowingIndicator.backgroundColor = [UIColor clearColor];
    glowingIndicator.image = [UIImage imageNamed:@"SBDockRunningIndicator"];
    glowingIndicator.alpha = 0;
    objc_setAssociatedObject(self, &kCSRunningGlowViewIdentifier, glowingIndicator, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addSubview:glowingIndicator];
    
    [self updateReflection];
    
    self.clipsToBounds = NO;

    return self;
}

%new;
- (void)updateReflection {
    CSReflectionView *reflectionView = objc_getAssociatedObject(self, &kCSReflectionViewIdentifier);
    CSReflectionView *reflectionOverlayView = objc_getAssociatedObject(self, &kCSReflectionOverlayViewIdentifier);
    UIView *glowingIndicator = objc_getAssociatedObject(self, &kCSRunningGlowViewIdentifier);
    if ([self isInDock]){
        reflectionView.image = [[self _iconImageView] snapshot];
        reflectionOverlayView.image = [[self _iconImageView] _currentOverlayImage];
        reflectionView.alpha = 0.4;
        if ([[self icon] isKindOfClass:[%c(SBApplicationIcon) class]]){
            SBApplicationIcon *appIcon = (SBApplicationIcon *)[self icon];
            SBApplication *app = [appIcon application];
            if ([app isRunning]){
                [UIView animateWithDuration:0.3 animations:^{
                    glowingIndicator.alpha = 1;
                }];
            } else {
                [UIView animateWithDuration:0.3 animations:^{
                    glowingIndicator.alpha = 0;
                }];
            }
            
        }
        if ([self isHighlighted])
            reflectionOverlayView.alpha = 0.3;
        else
            reflectionOverlayView.alpha = 0;
    } else {
        reflectionView.image = nil;
        reflectionView.alpha = 0;
        reflectionOverlayView.alpha = 0;
        glowingIndicator.alpha = 0;
    }
    CGRect frame = reflectionView.frame;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        frame.origin.x = [self _iconImageView].frame.origin.x;
        frame.origin.y = 77;
        frame.size.height = [UIImage imageNamed:@"SBDockReflectionHeight"].size.height-21;
        frame.size.width = reflectionView.image.size.width;
    } else {
        frame.origin.x = [self _iconImageView].frame.origin.x;
        frame.origin.y = 59;
        frame.size.height = [UIImage imageNamed:@"SBDockReflectionHeight"].size.height-21;
        frame.size.width = reflectionView.image.size.width;
    }
    reflectionView.frame = frame;
    reflectionOverlayView.frame = frame;
    if (!CSReflectionEnabled){
        reflectionView.alpha = 0;
        reflectionOverlayView.alpha = 0;
    }
    if (!CSRunningIndicatorsEnabled)
        glowingIndicator.alpha = 0;
}

- (void)setHighlighted:(BOOL)highlighted {
    %orig;
    [self updateReflection];
}

#define GET_POS(VIEW) (VIEW.frame.origin.y + VIEW.frame.size.height/2)
%new;
- (void)bounceWithMagnitude:(float)height duration:(float)duration completion:(void (^)())completion {
    SBIconImageView *iconView = [self _iconImageView];
    CSReflectionView *reflectionView = objc_getAssociatedObject(self, &kCSReflectionViewIdentifier);
    UIView *accessoryView = [self valueForKey:@"_accessoryView"];

    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanFalse forKey:kCATransactionDisableActions];
    [CATransaction setValue:[NSNumber numberWithFloat:duration] forKey:kCATransactionAnimationDuration];

    BounceAnimation *iconAnimation = [BounceAnimation
                                            animationWithKeyPath:@"position.y"
                                            minValue:GET_POS(iconView)
                                            maxValue:(GET_POS(iconView) - height)];
    [iconView.layer addAnimation:iconAnimation forKey:@"position"];
    BounceAnimation *reflectionAnimation = [BounceAnimation
                                            animationWithKeyPath:@"position.y"
                                            minValue:GET_POS(reflectionView)
                                            maxValue:(GET_POS(reflectionView) + height)];
    [reflectionView.layer addAnimation:reflectionAnimation forKey:@"position"];
    BounceAnimation *accessoryAnimation = [BounceAnimation
                                            animationWithKeyPath:@"position.y"
                                            minValue:GET_POS(accessoryView)
                                            maxValue:(GET_POS(accessoryView) - height)];
    [accessoryView.layer addAnimation:accessoryAnimation forKey:@"position"];

    [CATransaction commit];

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), completion);
}

%end

%hook SBApplication
- (void)setRunning:(BOOL)running {
    %orig;
    SBIconModel *model = (SBIconModel *)[[%c(SBIconController) sharedInstance] model];
    SBIcon *icon = [model expectedIconForDisplayIdentifier:[self displayIdentifier]];
    SBIconView *iconView = [[%c(SBIconViewMap) homescreenMap] mappedIconViewForIcon:icon];
    [iconView updateReflection];
    
}
- (void)_setRunning:(BOOL)running {
    %orig;
    SBIconModel *model = (SBIconModel *)[[%c(SBIconController) sharedInstance] model];
    SBIcon *icon = [model expectedIconForDisplayIdentifier:[self displayIdentifier]];
    SBIconView *iconView = [[%c(SBIconViewMap) homescreenMap] mappedIconViewForIcon:icon];
    [iconView updateReflection];
}
%end

%hook SBApplicationIcon
- (void)launchFromLocation:(int)location {
    if (CSBouncedAlready){
        %orig;
        CSBouncedAlready = NO;
        return;
    }
    SBIconView *iconView = [[%c(SBIconViewMap) homescreenMap] mappedIconViewForIcon:self];
    if ([iconView isInDock] && CSBounceIconOnLaunch){
        [iconView bounceWithMagnitude:30.0f duration:0.5f completion:^{
            CSBouncedAlready = YES;
            [self launchFromLocation:location];
         }];
    } else {
        %orig;
    }
}
%end

%hook SBIconImageView
- (void)setOverlayAlpha:(float)alpha {
    %orig;
    if ([self.superview isKindOfClass:[%c(SBIconView) class]])
        [(SBIconView *)self.superview updateReflection];
}

- (void)_updateOverlayAlpha {
    %orig;
    if ([self.superview isKindOfClass:[%c(SBIconView) class]])
        [(SBIconView *)self.superview updateReflection];
}

- (void)layoutSubviews {
    %orig;
    if ([self.superview isKindOfClass:[%c(SBIconView) class]])
        [(SBIconView *)self.superview updateReflection];
}

- (void)setIcon:(id)icon animated:(BOOL)animated {
    %orig;
    if ([self.superview isKindOfClass:[%c(SBIconView) class]])
        [(SBIconView *)self.superview updateReflection];
}
%end

%hook SBIconListPageControl
- (id)initWithFrame:(CGRect)frame {
    frame.origin.y += 15;
    return %orig(frame);
}

- (void)setFrame:(CGRect)frame {
    frame.origin.y += 15;
    %orig(frame);
}
%end

%hook SBHighlightView
-(id)initWithFrame:(CGRect)frame highlightAlpha:(float)alpha highlightHeight:(float)alpha2 {
    self = %orig;
    self.alpha=0;
    return self;
}

- (void)setAlpha:(float)alpha {
    %orig(0);
}
%end
%end

%ctor {

    NSDictionary *prefs = [NSDictionary dictionaryWithContentsOfFile:[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Preferences/org.coolstar.classicdock.plist"]];
        
    BOOL enabled = [prefs objectForKey:@"enabled"] ? [[prefs objectForKey:@"enabled"] boolValue] : YES;
    CSReflectionEnabled = [prefs objectForKey:@"reflectIcons"] ? [[prefs objectForKey:@"reflectIcons"] boolValue] : YES;
    CSRunningIndicatorsEnabled = [prefs objectForKey:@"runningIndicators"] ? [[prefs objectForKey:@"runningIndicators"] boolValue] : YES;
    CSDarkenBackground = [prefs objectForKey:@"darkenBackground"] ? [[prefs objectForKey:@"darkenBackground"] boolValue] : YES;
    CSBounceIconOnLaunch = [prefs objectForKey:@"bounceLaunch"] ? [[prefs objectForKey:@"bounceLaunch"] boolValue] : NO;

    if (enabled)
        %init(ClassicDock);

}
