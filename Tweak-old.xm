#include <substrate.h>
#include <UIKit/UIKit.h>
#include <objc/runtime.h>

@interface _SBDockBackgroundView : UIView

@end

@interface SBWallpaperEffectView : UIView

@end

@interface SBHighlightView : UIView
@end

@interface SBIconController : NSObject
+ (id)sharedInstance;
- (BOOL)isEditing;
@end

@interface SBDockView : UIView

- (UIView *)dockListView;
- (void)updateReflection:(id)sender;
- (void)setShouldUpdateReflection;

@end

@class SBWallpaperEffectView;

%hook _SBDockBackgroundView

-(id)initWithMaskImage:(UIImage *)maskImage {
    
    self = %orig(nil);
    
    if (self) {
        
        MSHookIvar<SBWallpaperEffectView *>(self, "_unmaskedView") = nil; //remove that mask view
        MSHookIvar<SBWallpaperEffectView *>(self, "_maskedView") = nil; //another view to remove
        
        maskImage = [UIImage imageNamed:@"SBDockBG.png"];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            maskImage = [UIImage imageNamed:@"SBDockBG-Landscape.png"];
        CGRect bgFrame = CGRectMake(0,51,self.frame.size.width,45);
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            bgFrame = CGRectMake(0,78,self.frame.size.width,45);
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:bgFrame];
        imgView.image = maskImage;
        imgView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview:[imgView autorelease]];
    }
    
    return self;
    
}

/*- (void)setAlpha:(float)alpha {
    %orig(1);
}*/

%end

static NSMutableDictionary *shouldUpdateDocks;
static NSMutableDictionary *reflectionViews;
static CADisplayLink *displayLink = nil;
static SBDockView *lastDockView = nil;

%ctor {
    reflectionViews = [[NSMutableDictionary alloc] init];
    shouldUpdateDocks = [[NSMutableDictionary alloc] init];
}

%hook SBDockView

- (id)initWithDockListView:(id)dockListView forSnapshot:(BOOL)snapshot {
    self = %orig;
    
    lastDockView = self;
    
    CGFloat offset = 50.f;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        offset = 65.f;
    
    CGRect dockFrame = CGRectOffset(self.bounds,0,offset);
    dockFrame.size.height = [UIImage imageNamed:@"SBDockReflectionHeight"].size.height;
    UIImageView *reflectionView = [[UIImageView alloc] initWithFrame:dockFrame];
        reflectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    reflectionView.contentMode = UIViewContentModeBottom;
    reflectionView.transform = CGAffineTransformMakeScale(1, -1);
    reflectionView.layer.zPosition = -1;
    reflectionView.alpha = 0.4;
    reflectionView.clipsToBounds = YES;
    [self addSubview:reflectionView];
#ifdef __LP64__
    [reflectionViews setObject:reflectionView forKey:[NSNumber numberWithLong:(long)self]];
    [shouldUpdateDocks setObject:@YES forKey:[NSNumber numberWithLong:(long)self]];
#else
    [reflectionViews setObject:reflectionView forKey:[NSNumber numberWithInt:(int)self]];
    [shouldUpdateDocks setObject:@YES forKey:[NSNumber numberWithInt:(int)self]];
#endif
    
    [displayLink invalidate];
    displayLink = nil;
    
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateReflection:)];
    displayLink.frameInterval = 30;
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [self updateReflection:nil];

    return self;
}

%new
- (void)setShouldUpdateReflection {
#ifdef __LP64__
    [shouldUpdateDocks setObject:@YES forKey:[NSNumber numberWithLong:(long)self]];
#else
    [shouldUpdateDocks setObject:@YES forKey:[NSNumber numberWithInt:(int)self]];
#endif
}

%new
- (void)updateReflection:(id)sender {
    BOOL shouldUpdateDock = [[%c(SBIconController) sharedInstance] isEditing];
    if (!shouldUpdateDock){
#ifdef __LP64__
        shouldUpdateDock = [[shouldUpdateDocks objectForKey:[NSNumber numberWithLong:(long)self]] boolValue];
        if (shouldUpdateDock){
            [shouldUpdateDocks setObject:@NO forKey:[NSNumber numberWithLong:(long)self]];
        }
#else
        shouldUpdateDock = [[shouldUpdateDocks objectForKey:[NSNumber numberWithInt:(int)self]] boolValue];
        if (shouldUpdateDock){
            [shouldUpdateDocks setObject:@NO forKey:[NSNumber numberWithInt:(int)self]];
        }
#endif
    }
    if (!shouldUpdateDock)
        return;
    
    dispatch_async(dispatch_get_main_queue(),^{
#ifdef __LP64__
        UIImageView *reflectionView = [reflectionViews objectForKey:[NSNumber numberWithLong:(long)self]];
#else
        UIImageView *reflectionView = [reflectionViews objectForKey:[NSNumber numberWithInt:(int)self]];
#endif
        UIGraphicsBeginImageContextWithOptions([self dockListView].bounds.size, NO, 0.0);
        [[self dockListView] drawViewHierarchyInRect:[self dockListView].bounds afterScreenUpdates:YES];
        //[[self dockListView].layer renderInContext:UIGraphicsGetCurrentContext()];
        reflectionView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    });
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *returnVal = %orig;
    if (returnVal == nil)
        return returnVal;
    [self setShouldUpdateReflection];
    [self updateReflection:nil];
    if (lastDockView != self){
        lastDockView = self;
        displayLink = [CADisplayLink displayLinkWithTarget:lastDockView selector:@selector(updateReflection:)];
        displayLink.frameInterval = 30;
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
    return returnVal;
}

/*- (void)setBackgroundAlpha:(float)alpha {
    %orig(1);
}*/

- (void)dealloc {
#ifdef __LP64__
    [reflectionViews removeObjectForKey:[NSNumber numberWithLong:(long)self]];
    [shouldUpdateDocks removeObjectForKey:[NSNumber numberWithLong:(long)self]];
#else
    [reflectionViews removeObjectForKey:[NSNumber numberWithInt:(int)self]];
    [shouldUpdateDocks removeObjectForKey:[NSNumber numberWithLong:(long)self]];
#endif
    if (lastDockView == self)
        lastDockView = nil;
    %orig;
}

%end

%hook SBLockScreenManager

- (void)updateSpringBoardStatusBarForLockScreenTeardown {
    %orig;
    
    [displayLink invalidate];
    displayLink = nil;
    
    if (lastDockView != nil){
        displayLink = [CADisplayLink displayLinkWithTarget:lastDockView selector:@selector(updateReflection:)];
        displayLink.frameInterval = 30;
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
}

%end

%hook SBHighlightView
-(id)initWithFrame:(CGRect)frame highlightAlpha:(float)alpha highlightHeight:(float)alpha2 {
    self = %orig;
    self.alpha=0;
    return self;
}

- (void)setAlpha:(float)alpha {
    %orig(0);
}
%end