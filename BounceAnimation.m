/*
Copyright (c) 2014 Reed Weichler

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#import "BounceAnimation.h"

@implementation BounceAnimation

+ (id)animationWithKeyPath:(NSString *)keyPath
	minValue:(double)minValue
	maxValue:(double)maxValue
{
	id animation = [self animationWithKeyPath:keyPath];

	[animation
		calculateKeyFramesWithMinValue:(double)minValue
		maxValue:(double)maxValue];
	
	return animation;
}

float parabola(float maxY, float x, float midX)
{
    return -maxY/(midX*midX)*(x-midX)*(x-midX) + maxY;
}

- (void)calculateKeyFramesWithMinValue:(double)minValue
	maxValue:(double)maxValue
{
	NSUInteger count = NUM_BOUNCE_ANIMATION_STEPS + 2;
	
	NSMutableArray *valueArray = [NSMutableArray arrayWithCapacity:count];
    
    long firstTime = count*2/3;
    long secondTime = count*1/3;
    long thirdTime = 0;
    
	for (int i = 0; i < count; i++)
	{
        double value = minValue;
        if(i < firstTime) //first come down
        {
            value += parabola(maxValue - minValue, i, firstTime/2);
        }
        else if(i < firstTime + secondTime) //second come up
        {
            value += parabola((maxValue - minValue)/3, i - firstTime, secondTime/2);
            
        }
        else //second come down
        {
            value += parabola((maxValue - minValue)/4, i - firstTime - secondTime, thirdTime/2);
            
        }
		//double value = 41.5 + i*velocity;//startValue + (velocity + acceleration*progress) * progress;
		[valueArray addObject:@(value)];
	}
	
	[self setValues:valueArray];
}

@end
