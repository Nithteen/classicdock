ARCHS ?= armv7 arm64
include theos/makefiles/common.mk

TWEAK_NAME = ClassicDock
ClassicDock_FILES = Tweak.xm BounceAnimation.m
ClassicDock_FRAMEWORKS = UIKit QuartzCore CoreGraphics

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"
SUBPROJECTS += classicdockprefs
include $(THEOS_MAKE_PATH)/aggregate.mk
