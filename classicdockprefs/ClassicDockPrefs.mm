@interface PSListController : UITableViewController {
    id _specifiers;
}
- (id)loadSpecifiersFromPlistName:(NSString *)plistName target:(id)target;
@end

@interface ClassicDockPrefsListController: PSListController {
}
@end

@implementation ClassicDockPrefsListController
- (id)specifiers {
	if(_specifiers == nil) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"ClassicDockPrefs" target:self] retain];
	}
	return _specifiers;
}
- (void)respring:(id)sender {
    system("killall backboardd && killall SpringBoard");
}
- (void)coolstarTwitter:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/coolstarorg"]];
}
- (void)sharedRoutineTwitter:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/sharedRoutine"]];
}
@end

// vim:ft=objc
