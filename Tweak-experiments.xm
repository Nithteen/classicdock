#include <substrate.h>
#include <UIKit/UIKit.h>
#include <objc/runtime.h>

@interface _SBDockBackgroundView : UIView

@end

@interface SBWallpaperEffectView : UIView

@end

@interface SBHighlightView : UIView
@end

@interface SBIconController : NSObject
+ (id)sharedInstance;
- (BOOL)isEditing;
@end

@interface SBDockIconListView : UIView

@end

@interface SBDockView : UIView

- (UIView *)dockListView;
- (void)updateReflection:(id)sender;
- (void)setShouldUpdateReflection;

@end

@class SBWallpaperEffectView;

%hook _SBDockBackgroundView

-(id)initWithMaskImage:(UIImage *)maskImage {
    
    self = %orig(nil);
    
    if (self) {
        
        MSHookIvar<SBWallpaperEffectView *>(self, "_unmaskedView") = nil; //remove that mask view
        MSHookIvar<SBWallpaperEffectView *>(self, "_maskedView") = nil; //another view to remove
        
        maskImage = [UIImage imageNamed:@"SBDockBG.png"];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            maskImage = [UIImage imageNamed:@"SBDockBG-Landscape.png"];
        CGRect bgFrame = CGRectMake(0,51,self.frame.size.width,45);
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            bgFrame = CGRectMake(0,78,self.frame.size.width,45);
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:bgFrame];
        imgView.image = maskImage;
        imgView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview:[imgView autorelease]];
    }
    
    return self;
    
}

- (void)setAlpha:(float)alpha {
    %orig(1);
}

%end

/*%hook SBDockIconListView

+ (Class)layerClass {
    return [CAReplicatorLayer class];
}

%end*/

%hook SBDockView

/*- (id)initWithDockListView:(id)dockListView forSnapshot:(BOOL)snapshot {
    self = %orig;
    
    CAReplicatorLayer *replicatorLayer = (CAReplicatorLayer *)[self dockListView].layer;
    replicatorLayer.opacity = 0.5;
    replicatorLayer.instanceTransform = CATransform3DTranslate(CATransform3DMakeScale(1.0, -1.0, 1.0),0.0,-50.0,0.0);
    replicatorLayer.instanceCount = 2;
    replicatorLayer.instanceAlphaOffset = 0.5;
    
    return self;
}*/

- (void)setBackgroundAlpha:(float)alpha {
    %orig(1);
}

%end

@interface SBIconImageView : UIImageView
@end

%hook SBIconImageView

+ (Class)layerClass {
    return [CAReplicatorLayer class];
}

- (id)initWithFrame:(CGRect)frame {
    self = %orig;
    
    self.clipsToBounds = NO;
    
    CAReplicatorLayer *replicatorLayer = (CAReplicatorLayer *)self.layer;
    replicatorLayer.instanceTransform = CATransform3DTranslate(CATransform3DMakeScale(1.0, -1.0, 1.0),0.0,-50.0,0.0);
    replicatorLayer.instanceCount = 2;
    replicatorLayer.instanceAlphaOffset = -0.5;
    return self;
}

%end

%hook SBIconListPageControl
- (id)initWithFrame:(CGRect)frame {
    frame.origin.y += 15;
    return %orig(frame);
}

- (void)setFrame:(CGRect)frame {
    frame.origin.y += 15;
    %orig(frame);
}
%end

%hook SBHighlightView
-(id)initWithFrame:(CGRect)frame highlightAlpha:(float)alpha highlightHeight:(float)alpha2 {
    self = %orig;
    self.alpha=0;
    return self;
}

- (void)setAlpha:(float)alpha {
    %orig(0);
}
%end